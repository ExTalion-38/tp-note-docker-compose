const express = require('express');
const mongoose = require('mongoose');

const app = express();

// Remplacez l'URL de connexion ci-dessous par l'URL de votre base de données MongoDB
const mongodbUrl = 'mongodb://localhost:27017';

mongoose.connect(mongodbUrl, { useNewUrlParser: true });

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
  // Créez un schéma pour votre collection
    const userSchema = new mongoose.Schema({
        id: Number,
        nom: String,
        age: Number,
        email: String,
    });

  // Créez un modèle à partir du schéma
    const Users = mongoose.model('Users', userSchema);

  // Exécutez une requête de sélection sur la collection
    Users.find({}, (error, users) => {
        if (error) {
          console.error(error);
          return;
        }
        console.log(users);
    });
});

app.listen(3000, () => {
    console.log('App écoute sur le port 3000');
});